import { Component, OnInit } from '@angular/core';
import { faTimes, faPlus, faPlusSquare } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-venta',
  templateUrl: './venta.component.html',
  styleUrls: ['./venta.component.css']
})
export class VentaComponent implements OnInit {

  faTimes = faTimes;
  faPlus = faPlus;
  faPlusSquare = faPlusSquare;

  tabIndex = 0;

  constructor() { }

  ngOnInit() {
  }

  siguiente () {
    this.tabIndex = 1;
  }

  regresar () {
    this.tabIndex = 0;
  }

}
